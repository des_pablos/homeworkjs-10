const input1 = document.querySelector('.input1');
const input2 = document.querySelector('.input2');
const password = document.querySelector('.icon-password');
const btn = document.querySelector('.btn');

function showPass(event) {
    if (event.target.classList.contains('icon-password')) {
        const input = event.target.previousElementSibling;
        if (input.type === 'password') {
            input.type = 'text';

        } else {
            input.type = 'password'
        }
        event.target.classList.toggle('fa-eye-slash');
        console.log(input);
    }
}

function verifyPassword() {
    if (input1.value === input2.value) {
        alert('You are welcome');
    } else {
        alert('Please, write correct password!');
    }
}

password.addEventListener('click', showPass);
btn.addEventListener('click', verifyPassword);